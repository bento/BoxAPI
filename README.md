# Box API 2.0 For Laravel 5.x

This is a package to used with Laravel 5.x or above or also for non Laravel project. This package will serve App User for Enterprise to communicate server to server (eg. Uploading files to company Box behind the scene without ask grant to your web visitor, and Standard User for common user accesing their Box Account, and this will need grant access from their Box Account.

## Installation

To instal into your project just add this into command line 

	composer require maengkom/boxapi
	
After download completed, you can add this to your app.php config file 

	Maengkom\Box\BoxAPIServiceProvider::class,
	
And if you want using Facade, you can use these two facade

	'BoxAU'     => Maengkom\Box\Facades\AppUserFacade::class,
	'BoxSU'     => Maengkom\Box\Facades\StandardUserFacade::class,
	
BoxAU is used for App User for Enterprise box account. BoxSU is used for normal box user to access their asset in their box account.

After this don't forget to run this command below, to copy config file into config folder in Laravel project

	publish vendor:publish --provider="Maengkom/Box/BoxAPIServiceProvider"

## Configuration

There are some configuration key to set in boxapi.php in config folder.

Please read the comment and open url box documentation to get the values for those keys.

For App User type, assumed you only need one user for your webserver communicate with your box account. Just set app\_user\_name and package will check if not exist, it will created for you based on name, if exist as app user on your box app, it will use user id to used in application.

## API List

Below are the API method you can used. All methods are following Box documentation.


 Name		| Method | Verb	| Url 
 ----- 	| -------| ----------- 	| ---- |
 Get Folder’s Info | getFolderInfo()	| Get	| https://api.box.com/2.0/folders/{FOLDER_ID} 
 Get Folder's Item |	getFolderItems()	| Get	| https://api.box.com/2.0/folders/{FOLDER_ID}/items
Create Folder	| createFolder() |	Post	| https://api.box.com/2.0	/folders
Update Folder | 	updateFolder() |	Put	| https://api.box.com/2.0	/folders/{FOLDER_ID}
Delete Folder |	deleteFolder()	| Delete |	https://api.box.com/2.0	/folders/{FOLDER_ID}
Copy Folder	| copyFolder()	| Post	| https://api.box.com/2.0	/folders/{FOLDER_ID}/copy
Create Shared Link	| createSharedLink() |	Put	| https://api.box.com/2.0	/folders/{FOLDER_ID}
Folder Collaborations | folderCollaborations() |	Get	| https://api.box.com/2.0	/folders/{FOLDER_ID}/collaborations
Get Trashed Items | getTrashedItems() |	Get |	https://api.box.com/2.0	/folders/trash/items
Get Trashed Folder	| getTrashedFolder() |	Get	| https://api.box.com/2.0	/folders/{FOLDER_ID}/trash
Permanently Delete	| permanentDelete()	| Delete |	https://api.box.com/2.0	/folders/{FOLDER_ID}/trash
Restore Folder	| restoreFolder()	| Get |	https://api.box.com/2.0	

## Example
If you want to get folder items in root, call this 

	BoxAU::getFolderItems(); // For App User 
	BoxSU::getFolderItems(); // For Standard User

	